import AbstractCrudService from './abstract-crud.service';
import PostsRepository from '../repositories/posts.repository';
import {Post} from "../models/post.model";
import {SearchOptions} from "../models/search-options.model";

export default class PostsService extends AbstractCrudService {
    constructor() {
        super(new PostsRepository('posts'));
    }

    async search(searchOptions: SearchOptions): Promise<Post[]> {
        const data = await this.repository.search(searchOptions, 'edges{node{createdAt description thumbnail{url} url name}}');
        return data.posts.edges.map(e => ({
            createdAt: e.node.createdAt,
            description: e.node.description,
            thumbnailUrl: e.node.thumbnail.url,
            url: e.node.url,
            name: e.node.name
        }))
    }

}
