import AbstractCrudRepository from "../repositories/abstract-crud.repository";
import {SearchOptions} from "../models/search-options.model";

export default class AbstractCrudService {
    repository: AbstractCrudRepository;

    constructor(repository: AbstractCrudRepository) {
        this.repository = repository;
    }

    async search(searchOptions: SearchOptions) {
        return this.repository.search(searchOptions, '');
    }

    // Cette classe est utile pour éviter la duplication de code
    // au niveau des services si on avait d'autres ressources a traiter

    // Je la laisse pour illustration mais elle n'est évidemmemnt pas pertinente ici

    // Sur une app classique, c'est ici qu'on aurait rajouté les fonctions de CRUD

}
