# Backend



## Lancer le backend

`npm install` puis `npm start`


## Lancer le lint

`npm run lint`

## Notes

J'aurais d'habitude mis le dossier credentials dans le .gitignore
pour ne pas pousser de tokens sur le repo mais je l'ai
laissé pour que vous puissiez tester l'app
