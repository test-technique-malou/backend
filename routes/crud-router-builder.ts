import {Request, Response} from "express";
import AbstractCrudService from "../services/abstract-crud.service";

export default function buildCrudRouter(router, service: AbstractCrudService) {
    router.get('/search', function (req: Request, res: Response) {
        service.search({
            postedAfter: req.query.startDate as string,
            postedBefore: req.query.endDate as string
        }).then(next => {
            res.status(200).send(next);
        }).catch((err) => {
            console.log(err);
            res.status(500).end();
        });
    })

    // Cette classe est utile pour éviter la duplication de code
    // au niveau des routes si on avait d'autres ressources a traiter

    // Je la laisse pour illustration mais elle n'est évidemmemnt pas pertinente ici

    // Sur une app classique, c'est ici qu'on aurait rajouté les fonctions de CRUD
}
