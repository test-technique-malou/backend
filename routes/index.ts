import express from 'express';
import PostsRoute from './posts.route';

const router = express.Router();

router.use('/posts', PostsRoute);

export default router;
