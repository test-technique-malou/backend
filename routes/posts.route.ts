import express from 'express';
import PostsService from '../services/posts.service';
import crudRouterBuilder from './crud-router-builder';


const router = express.Router();

crudRouterBuilder(router, new PostsService());

export default router;
