import express from 'express';
import Routes from './routes';
import cors from 'cors';

const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(Routes);

app.listen(port, () => {
  console.log(`App listening on port: ${port}`)
})
