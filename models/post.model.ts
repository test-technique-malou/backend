export interface Post {
    createdAt?: Date,
    description?: Date
    thumbnailUrl?: string;
    url?: string;
    name?: string;
}
