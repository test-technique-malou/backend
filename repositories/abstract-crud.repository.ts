import {gql, GraphQLClient} from "graphql-request";
import productHuntCredentials from '../credentials/product-hunt-credentials';
import {SearchOptions} from "../models/search-options.model";

export default class AbstractCrudRepository {

    graphQLClient: GraphQLClient;
    rootObject: string;

    constructor(rootObject) {
        this.rootObject = rootObject;
        this.graphQLClient = new GraphQLClient(productHuntCredentials.url, {
            headers: {
                authorization: `Bearer ${productHuntCredentials.developer_token}`,
            },
        })
    }

    async search(searchOptions: SearchOptions, projection: string) {
        const query = this._graphQlBuilder(this.rootObject, searchOptions ?? {}, projection);
        const graphQlQuery = gql`${query}`;
        return await this.graphQLClient.request(graphQlQuery);
    }

    _graphQlBuilder(rootObject, search: SearchOptions, projection: string) {
        let searchQuery = '';
        for (const key of Object.keys(search)) {
            if (search[key] !== undefined) {
                searchQuery += `${key}: "${search[key]}"`;
            }
        }
        return `{${rootObject}(${searchQuery}){${projection}}}`;
    }


    // Cette classe est utile pour éviter la duplication de code
    // au niveau des repositories si on avait d'autres ressources a traiter

    // Je la laisse pour illustration mais elle n'est évidemmemnt pas pertinente ici

    // Sur une app classique, c'est ici qu'on aurait rajouté les fonctions de CRUD

}
