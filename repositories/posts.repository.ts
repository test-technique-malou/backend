import AbstractCrudRepository from './abstract-crud.repository';


export default class PostsRepository extends AbstractCrudRepository {
    constructor(rootObject) {
        super(rootObject);
    }
}

